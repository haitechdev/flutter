# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170825051525) do

  create_table "images", force: :cascade do |t|
    t.string   "file_id",    limit: 255
    t.integer  "listing_id", limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "images", ["listing_id"], name: "index_images_on_listing_id", using: :btree

  create_table "listings", force: :cascade do |t|
    t.string   "title",             limit: 255
    t.integer  "price",             limit: 4
    t.integer  "year",              limit: 4
    t.string   "make",              limit: 255
    t.string   "model",             limit: 255
    t.string   "model_detail",      limit: 255
    t.integer  "kilometres",        limit: 4
    t.string   "ext_colout",        limit: 255
    t.string   "int_colour",        limit: 255
    t.integer  "no_doors",          limit: 4
    t.string   "body_style",        limit: 255
    t.string   "fuel_type",         limit: 255
    t.string   "cylinders",         limit: 255
    t.integer  "engine_size",       limit: 4
    t.string   "transmission",      limit: 255
    t.boolean  "off_road"
    t.integer  "no_owners",         limit: 4
    t.string   "import_history",    limit: 255
    t.date     "rego_expiry"
    t.date     "wof_expiry"
    t.string   "no_plate",          limit: 255
    t.boolean  "aa_inspection"
    t.text     "stereo_descripton", limit: 65535
    t.boolean  "abs_brakes"
    t.boolean  "air_con"
    t.boolean  "alarm"
    t.boolean  "alloy_wheels"
    t.boolean  "central_locking"
    t.boolean  "driver_airbag"
    t.boolean  "passenger_airbag"
    t.boolean  "power_steering"
    t.boolean  "sunroof"
    t.boolean  "towbar"
    t.text     "description",       limit: 65535
    t.integer  "user_id",           limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "listings", ["user_id", "created_at"], name: "index_listings_on_user_id_and_created_at", using: :btree
  add_index "listings", ["user_id"], name: "index_listings_on_user_id", using: :btree

  create_table "relationships", force: :cascade do |t|
    t.integer  "follower_id", limit: 4
    t.integer  "followed_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "relationships", ["followed_id"], name: "index_relationships_on_followed_id", using: :btree
  add_index "relationships", ["follower_id", "followed_id"], name: "index_relationships_on_follower_id_and_followed_id", unique: true, using: :btree
  add_index "relationships", ["follower_id"], name: "index_relationships_on_follower_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "username",               limit: 255
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.integer  "phone",                  limit: 4
    t.string   "street_no",              limit: 255
    t.string   "street_name",            limit: 255
    t.string   "street_type",            limit: 255
    t.string   "suburb",                 limit: 255
    t.string   "town_city",              limit: 255
    t.string   "region",                 limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  add_foreign_key "images", "listings"
  add_foreign_key "listings", "users"
end
