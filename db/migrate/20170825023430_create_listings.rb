class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
      t.string :title
      t.integer :price
      t.integer :year
      t.string :make
      t.string :model
      t.string :model_detail
      t.integer :kilometres
      t.string :ext_colout
      t.string :int_colour
      t.integer :no_doors
      t.string :body_style
      t.string :fuel_type
      t.string :cylinders
      t.integer :engine_size
      t.string :transmission
      t.boolean :off_road
      t.integer :no_owners
      t.string :import_history
      t.date :rego_expiry
      t.date :wof_expiry
      t.string :no_plate
      t.boolean :aa_inspection
      t.text :stereo_descripton
      t.boolean :abs_brakes
      t.boolean :air_con
      t.boolean :alarm
      t.boolean :alloy_wheels
      t.boolean :central_locking
      t.boolean :driver_airbag
      t.boolean :passenger_airbag
      t.boolean :power_steering
      t.boolean :sunroof
      t.boolean :towbar
      t.text :description
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :listings, [:user_id, :created_at]
  end
end
