class AddToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :phone, :integer
    add_column :users, :street_no, :string
    add_column :users, :street_name, :string
    add_column :users, :street_type, :string
    add_column :users, :suburb, :string
    add_column :users, :town_city, :string
    add_column :users, :region, :string
    
  end
end