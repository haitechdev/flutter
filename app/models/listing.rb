class Listing < ActiveRecord::Base
    belongs_to :user
    has_many :images, dependent: :destroy
    accepts_attachments_for :images, attachment: :file, append: true
    validates :user_id, presence: true
    default_scope -> { order(created_at: :desc) }
end