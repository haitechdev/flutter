# THe pages controller contains all of the code for any page inside /pages
class PagesController < ApplicationController
  def index
  end

  def explore
    if current_user != nil
      @followers_count = current_user.followers.count
      @following_count = current_user.following.count
      @user_posts = Listing.all.where("user_id = ?", current_user.id)
      @user_posts_count = @user_posts.length
      @username = current_user.username
    else
      redirect_to '/join/'
    end
    
    @posts = Listing.all
    @posts_count = @posts.length
    
    @newPost = Listing.new
  end

  def profile
    if(User.find_by_username(params[:id]))
      @username = params[:id]
      
      @followers_count = User.find_by_username(@username).followers.count
      @following_count = User.find_by_username(@username).following.count
        
      @posts = Listing.all.where("user_id = ?", User.find_by_username(@username).id)
      @posts_count = @posts.length
      @newPost = Listing.new
    else
      redirect_to '/explore/', :notice=> "User not found!"
    end
  end
end
