class ListingsController < ApplicationController
  before_action :set_listing, only: [:show, :edit, :update, :destroy]

  # GET /listings
  # GET /listings.json
  def index
    @listings = Listing.all
  end

  # GET /listings/1
  # GET /listings/1.json
  def show
  end

  # GET /listings/new
  def new
    @listing = Listing.new
  end

  # GET /listings/1/edit
  def edit
  end

  # POST /listings
  # POST /listings.json
  def create
    @listing = Listing.new(listing_params)
    @listing.user_id = current_user.id

    respond_to do |format|
      if @listing.save
        format.html { redirect_to "/user/" + current_user.username, notice: 'Listing was successfully created.' }
      else
        format.html { render :new }
        format.json { render json: @listing.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /listings/1
  # PATCH/PUT /listings/1.json
  def update
    respond_to do |format|
      if @listing.update(listing_params)
        f.html { redirect_to "/user/" + current_user.username, notice: "Listing was successfully updated!" }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /listings/1
  # DELETE /listings/1.json
  def destroy
    @listing.destroy
    respond_to do |format|
      f.html { redirect_to "/user/" + current_user.username, notice: "Post Deleted!" }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_listing
      @listing = Listing.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def listing_params
      params.require(:listing).permit(:title, :year, :make, :model, :model_detail, :kilometres, :ext_colout, :int_colour, :no_doors, :body_style, :fuel_type, :cylinders, :engine_size, :transmission, :off_road, :no_owners, :import_history, :rego_expiry, :wof_expiry, :no_plate, :aa_inspection, :stereo_descripton, :abs_brakes, :air_con, :alarm, :alloy_wheels, :central_locking, :driver_airbag, :passenger_airbag, :power_steering, :sunroof, :towbar, :description, images_files: [])
    end
end
