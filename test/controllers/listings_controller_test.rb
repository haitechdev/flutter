require 'test_helper'

class ListingsControllerTest < ActionController::TestCase
  setup do
    @listing = listings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:listings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create listing" do
    assert_difference('Listing.count') do
      post :create, listing: { 4wd: @listing.4wd, aa_inspection: @listing.aa_inspection, abs_brakes: @listing.abs_brakes, air_con: @listing.air_con, alarm: @listing.alarm, alloy_wheels: @listing.alloy_wheels, body_style: @listing.body_style, central_locking: @listing.central_locking, cylinders: @listing.cylinders, description: @listing.description, driver_airbag: @listing.driver_airbag, engine_size: @listing.engine_size, ext_colout: @listing.ext_colout, fuel_type: @listing.fuel_type, import_history: @listing.import_history, int_colour: @listing.int_colour, kilometres: @listing.kilometres, make: @listing.make, model: @listing.model, model_detail: @listing.model_detail, no_doors: @listing.no_doors, no_owners: @listing.no_owners, no_plate: @listing.no_plate, passenger_airbag: @listing.passenger_airbag, power_steering: @listing.power_steering, rego_expiry: @listing.rego_expiry, stereo_descripton: @listing.stereo_descripton, sunroof: @listing.sunroof, title: @listing.title, towbar: @listing.towbar, transmission: @listing.transmission, wof_expiry: @listing.wof_expiry, year: @listing.year }
    end

    assert_redirected_to listing_path(assigns(:listing))
  end

  test "should show listing" do
    get :show, id: @listing
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @listing
    assert_response :success
  end

  test "should update listing" do
    patch :update, id: @listing, listing: { 4wd: @listing.4wd, aa_inspection: @listing.aa_inspection, abs_brakes: @listing.abs_brakes, air_con: @listing.air_con, alarm: @listing.alarm, alloy_wheels: @listing.alloy_wheels, body_style: @listing.body_style, central_locking: @listing.central_locking, cylinders: @listing.cylinders, description: @listing.description, driver_airbag: @listing.driver_airbag, engine_size: @listing.engine_size, ext_colout: @listing.ext_colout, fuel_type: @listing.fuel_type, import_history: @listing.import_history, int_colour: @listing.int_colour, kilometres: @listing.kilometres, make: @listing.make, model: @listing.model, model_detail: @listing.model_detail, no_doors: @listing.no_doors, no_owners: @listing.no_owners, no_plate: @listing.no_plate, passenger_airbag: @listing.passenger_airbag, power_steering: @listing.power_steering, rego_expiry: @listing.rego_expiry, stereo_descripton: @listing.stereo_descripton, sunroof: @listing.sunroof, title: @listing.title, towbar: @listing.towbar, transmission: @listing.transmission, wof_expiry: @listing.wof_expiry, year: @listing.year }
    assert_redirected_to listing_path(assigns(:listing))
  end

  test "should destroy listing" do
    assert_difference('Listing.count', -1) do
      delete :destroy, id: @listing
    end

    assert_redirected_to listings_path
  end
end
